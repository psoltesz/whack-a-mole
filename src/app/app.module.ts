import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { MoleComponent } from './components/mole/mole.component';
import * as gameState from './state/game/game.reducer';

@NgModule({
  declarations: [AppComponent, MoleComponent],
  imports: [BrowserModule, StoreModule.forRoot({ game: gameState.reducer })],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
