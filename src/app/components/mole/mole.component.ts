import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable, Subject, interval, map, concatMap, race, of, delay, tap, take } from 'rxjs';

@Component({
  selector: 'app-mole',
  templateUrl: './mole.component.html',
  styleUrls: ['./mole.component.scss'],
})
export class MoleComponent implements OnInit {
  moleLifecycle$: Observable<boolean> = new Observable();
  moleClicked$ = new Subject();
  @Output() moleHit = new EventEmitter();
  @Output() moleMissed = new EventEmitter();

  ngOnInit(): void {
    this.moleLifecycle$ = interval().pipe(
      map((i) => i % 2 == 0),
      concatMap((shouldBeVisibleNext) => {
        // if a mole should spawn (at the beginning or after a successful whacking), the timeout is 5s - otherwise 3s
        const phaseLength = this.generateRandomInteger(1000, shouldBeVisibleNext ? 5000 : 3000);
        return race(
          of(shouldBeVisibleNext).pipe(
            delay(phaseLength),
            tap(() => {
              if (!shouldBeVisibleNext) {
                this.moleMissed.emit();
              }
            })
          ),
          this.moleClicked$.pipe(
            take(1),
            tap(() => this.moleHit.emit()),
            map(() => shouldBeVisibleNext)
          )
        );
      })
    );
  }

  moleClicked() {
    this.moleClicked$.next(true);
  }

  private generateRandomInteger(min: number, max: number) {
    return Math.floor(min + Math.random() * (max + 1 - min));
  }
}
