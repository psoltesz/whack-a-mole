import { Action, createReducer, on } from '@ngrx/store';
import * as fromActions from './game.action';
import { GameState } from './game.state';

export const initialState: GameState = { stage: 1, currentScore: 0, highScore: 0 };

const gameReducer = createReducer(
  initialState,
  on(fromActions.StartGame, (state) => {
    return { ...state, stage: 2, currentScore: 0 };
  }),
  on(fromActions.GameOver, (state) => {
    return { ...state, stage: 3 };
  }),
  on(fromActions.MoleHit, (state) => {
    const newScore = state.currentScore + 1;
    return { ...state, currentScore: newScore, highScore: Math.max(newScore, state.highScore) };
  }),
  on(fromActions.MoleMissed, (state) => {
    return { ...state, currentScore: state.currentScore - 1 };
  })
);

export function reducer(state: GameState | undefined, action: Action) {
  return gameReducer(state, action);
}
