export interface GameState {
  stage: number;
  currentScore: number;
  highScore: number;
}
