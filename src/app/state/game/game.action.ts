import { createAction } from '@ngrx/store';

export const StartGame = createAction('[ Stage 1 ] Start game');
export const GameOver = createAction('[ Stage 3 ] Game over');
export const MoleHit = createAction('[ Stage 2 ] Mole hit successfully');
export const MoleMissed = createAction('[ Stage 2 ] Mole missed');
