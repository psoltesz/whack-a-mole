import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { exhaustMap, filter, finalize, map, Observable, startWith, takeWhile, timer } from 'rxjs';
import { GameState } from './state/game/game.state';
import * as actions from './state/game/game.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'whack-a-mole';

  state$ = this.store.pipe(select('game'));
  gameLifecycle$ = new Observable();
  numberOfMoles = 6; // increase for harder difficulty
  gameLength = 30; // in seconds

  constructor(private store: Store<{ game: GameState }>) {}

  ngOnInit(): void {
    this.gameLifecycle$ = this.state$.pipe(
      map((state) => state.stage),
      filter((stage) => stage === 2),
      exhaustMap(() =>
        timer(0, 1000).pipe(
          map((i) => this.gameLength - i),
          takeWhile((i) => i >= 0),
          finalize(() => this.store.dispatch(actions.GameOver()))
        )
      ),
      startWith(this.gameLength)
    );
  }

  startGame() {
    this.store.dispatch(actions.StartGame());
  }

  moleHit() {
    this.store.dispatch(actions.MoleHit());
  }

  moleMissed() {
    this.store.dispatch(actions.MoleMissed());
  }
}
